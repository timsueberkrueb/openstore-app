# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-19 14:17+0000\n"
"PO-Revision-Date: 2017-12-10 22:42+0000\n"
"Last-Translator: Brînzariu Cristian <info@ebeton.ro>\n"
"Language-Team: Romanian <https://translate.ubports.com/projects/openstore/"
"openstore-app/ro/>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Weblate 2.15\n"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:45
msgid "App details"
msgstr "Detalii aplicație"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:52
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:279
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:767
msgid "Remove"
msgstr "Elimină"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:87
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:17
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:60
#, fuzzy
msgid "App"
msgstr "Aplicațiile mele"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:88
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:50
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:18
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:61
msgid "Scope"
msgstr "Scope"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:89
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:19
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:62
msgid "Web App"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:90
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:20
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:63
msgid "Web App+"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:125
msgid ""
"The installed version of this app doesn't come from the OpenStore server. "
"You can install the latest stable update by tapping the button below."
msgstr ""
"Versiunea instalată a acestei aplicații nu provine din serverul OpenStore. "
"Poți să instalezi ultima versiune oficială prin apăsara butonului de mai jos."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:150
msgid "Open"
msgstr "Deschide"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:167
msgid "The OpenStore is installed!"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:176
#, fuzzy
msgid "Install stable version"
msgstr "Versiunea instalată"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:179
msgid "Upgrade"
msgstr "Actualizează"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:182
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:738
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:296
msgid "Install"
msgstr "Instalează"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:265
msgid "This app is not compatible with your system."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:306
msgid "This app has access to restricted system data, see below for details."
msgstr ""
"Această aplicație are acces la informații protejate din sistem, vezi "
"detaliile mai jos."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:330
msgid "Description"
msgstr "Descriere"

#. TRANSLATORS: Title of the changelog section
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:353
msgid "What's New"
msgstr "Ce este nou"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:372
msgid "Packager/Publisher"
msgstr "Packager / Dezvoltator"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:373
msgid "OpenStore team"
msgstr "Echipa OpenStore"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:381
msgid "Installed version"
msgstr "Versiunea instalată"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:390
msgid "Latest available version"
msgstr "Cea mai nouă versiune disponibilă"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:399
msgid "First released"
msgstr "Data apariției"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:410
msgid "Downloads of the latest version"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:419
msgid "Total downloads"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:428
msgid "License"
msgstr "Licență"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:429
msgid "N/A"
msgstr "Nu este disponibil"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:439
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:106
msgid "Source Code"
msgstr "Codul sursă"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:452
msgid "Get support for this app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:465
msgid "Donate to support this app"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the packages from the same author. %1 is the name of the author.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:481
#, qt-format
msgid "More from %1"
msgstr "Mai multe de la %1"

#. TRANSLATORS: This is the button that shows a list of all the other packages in the same category. %1 is the name of the category.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:496
#, qt-format
msgid "Other apps in %1"
msgstr "Alte aplicații în %1"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:502
msgid "Package contents"
msgstr "Pachetul conține"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:595
msgid "Permissions"
msgstr "Permisiuni"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:600
#, fuzzy
msgid "Accounts"
msgstr "Furnizor de conturi"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:601
msgid "Audio"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:602
msgid "Bluetooth"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:603
msgid "Calendar"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:604
msgid "Camera"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:605
msgid "Connectivity"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:606
msgid "Contacts"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:607
msgid "Content Exchange Source"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:608
#, fuzzy
msgid "Content Exchange"
msgstr "Manipulator Hub de conținut"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:609
msgid "Debug"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:610
msgid "History"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:611
msgid "In App Purchases"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:612
msgid "Keep Display On"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:613
#, fuzzy
msgid "Location"
msgstr "Aplicație"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:614
msgid "Microphone"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:615
msgid "Read Music Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:616
msgid "Music Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:617
msgid "Networking"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:618
msgid "Read Picture Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:619
msgid "Picture Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:620
msgid "Push Notifications"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:621
msgid "Sensors"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:622
msgid "User Metrics"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:623
msgid "Read Video Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:624
msgid "Video Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:625
msgid "Video"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:626
msgid "Webview"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:630
msgid "Full System Access"
msgstr ""

#. TRANSLATORS: this will show when an app doesn't need any special permissions
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:635
msgid "none required"
msgstr "niciuna necesară"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:671
msgid "Read paths"
msgstr "Citește locațiile"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:688
msgid "Write paths"
msgstr "Scrie locațiile"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:703
msgid "Donating"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:704
msgid "Would you like to support this app with a donation to the developer?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:710
msgid "Donate now"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:718
msgid "Maybe later"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:731
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:235
msgid "Warning"
msgstr "Avertisment"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:732
msgid ""
"This app has access to restricted parts of the system and all of your data. "
"While the OpenStore maintainers have reviewed the code for this app for "
"safety, they are not responsible for anything bad that might happen to your "
"device or data from installing this app."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:746
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:776
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:304
#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:82
msgid "Cancel"
msgstr "Anulează"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:759
msgid "Remove package"
msgstr "Elimină pachetul"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:760
#, qt-format
msgid "Do you want to remove %1?"
msgstr "Vrei să elimini %1?"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:793
#, qt-format
msgid "%1 GB"
msgstr "%1 gigabiți"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:798
#, qt-format
msgid "%1 MB"
msgstr "%1 megabiți"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:803
#, qt-format
msgid "%1 kB"
msgstr "%1 kilobiți"

#. TRANSLATORS: %1 is the size of a file, expressed in bytes
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:806
#, qt-format
msgid "%1 bytes"
msgstr "%1 biți"

#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesPage.qml:24
#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesTab.qml:90
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:36
msgid "Categories"
msgstr "Categorii"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:29
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:35
msgid "Discover"
msgstr "Descoperă"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:51
#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:28
msgid "Search in OpenStore..."
msgstr "Caută în OpenStore..."

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:136
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:79
msgid "Update available"
msgstr "Actualizare disponibilă"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:137
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:79
msgid "✓ Installed"
msgstr "Instalată"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:156
msgid "OpenStore update available"
msgstr "Actualizare OpenStore disponibilă"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:157
msgid "Update OpenStore now!"
msgstr "Actualizează OpenStore acum!"

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:163
msgid "Details"
msgstr "Detalii"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "Nothing here yet"
msgstr "Încă nu există nimic aici"

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "No results found."
msgstr "Nu am găsit niciun rezultat."

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "No app has been released in this category yet."
msgstr "Nu a fost publicată nicio aplicație în această categorie până acum."

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "Try with a different search."
msgstr "Încearcă să folosești o căutare diferită."

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:50
#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:38
msgid "My Apps"
msgstr "Aplicațiile mele"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:55
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:26
msgid "Settings"
msgstr "Setări"

#. TRANSLATORS: %1 is the number of installed apps
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:91
#, qt-format
msgid "Installed apps (%1)"
msgstr "Aplicații instalate (%1)"

#. TRANSLATORS: %1 is the number of available app updates
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:95
#, qt-format
msgid "Available updates (%1)"
msgstr "Actualizări disponibile (%1)"

#. TRANSLATORS: %1 is the number of apps that can be downgraded
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:99
#, qt-format
msgid "Stable version available (%1)"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:102
#, fuzzy
msgid ""
"The installed versions of these apps did not come from the OpenStore but a "
"stable version is available."
msgstr ""
"Versiunea instalată a acestei aplicații nu provine din serverul OpenStore. "
"Poți să instalezi ultima versiune oficială prin apăsara butonului de mai jos."

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:103
#, fuzzy
msgid "Update all"
msgstr "Actualizare disponibilă"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:188
msgid "No apps found"
msgstr "Nici o aplicație găsită"

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:189
msgid "No app has been installed from OpenStore yet."
msgstr "Nu ai încă nicio aplicație instalată din OpenStore."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:244
msgid ""
"You are currently using a non-standard domain for the OpenStore. This is a "
"development feature. The domain you are using is:"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:258
msgid "Are you sure you want to continue?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:262
msgid "Yes, I know what I'm doing"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:270
msgid "Get me out of here!"
msgstr "Scoate-mă de aici!"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:282
#, fuzzy
msgid "Install unknown app?"
msgstr "Instalezi aplicația?"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:283
#, fuzzy, qt-format
msgid "Do you want to install the unkown app %1?"
msgstr "Vrei să instalezi %1?"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:324
msgid "App installed"
msgstr "Aplicație instalată"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:325
msgid "The app has been installed successfully."
msgstr "Aplicația a fost instalată cu succes."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:328
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:341
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:356
msgid "OK"
msgstr "OK"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:337
msgid "Installation failed"
msgstr "Instalarea a eșuat"

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:338
msgid ""
"The package could not be installed. Make sure it is a valid click package."
msgstr ""
"Pachetul nu sa instalat.\n"
"Verifică dacă este un pachet de tip click valid."

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:352
#, qt-format
msgid "Installation failed (Error %1)"
msgstr "Instalarea a eșuat (Eroare %1)"

#: /home/brian/dev/openstore/openstore-app/openstore/MainPage.qml:37
#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:15
msgid "Search"
msgstr "Caută"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:29
msgid "Your passphrase is required to access restricted content"
msgstr ""
"Fraza ta parolă este necesară pentru accesarea conținutului restricționat"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:30
msgid "Your passcode is required to access restricted content"
msgstr ""
"Codul tău de tip parolă este necesar pentru accesarea conținutului "
"restricționat"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:39
msgid "Authentication failed"
msgstr "Autentificare eșuată"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passphrase required"
msgstr "Frază de tip parolă necesară"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passcode required"
msgstr "Este necesară parola"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passphrase (default is 0000)"
msgstr "frază parolă (implicit este 0000)"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passcode (default is 0000)"
msgstr "cod parolă (implicit este 0000)"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:71
msgid "Authentication failed. Please retry"
msgstr "Autentificare eșuată. Te rugăm să încerci din nou"

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:76
msgid "Authenticate"
msgstr "Autentificare"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:58
msgid "Parental control"
msgstr "Control parental"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:64
msgid "Hide adult-oriented content"
msgstr "Ascunde conținutul pentru adulți"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:74
#, fuzzy
msgid ""
"By typing your password you take full responsibility for showing NSFW "
"content."
msgstr ""
"Prin tastarea parolei îți asumi întreaga responsabilitate pentru afișarea "
"conținutului pentru adulți."

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:99
msgid "About OpenStore"
msgstr "Despre OpenStore"

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:115
msgid "Report an issue"
msgstr "Raportează o problemă"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:92
msgid "Back"
msgstr "Înapoi"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:92
msgid "Close"
msgstr "Închide"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:37
msgid "Application"
msgstr "Aplicație"

#. TRANSLATORS: This is an Ubuntu platform service for launching other applications (ref. https://developer.ubuntu.com/en/phone/platform/guides/url-dispatcher-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:64
msgid "URL Handler"
msgstr "Manipulator de URL"

#. TRANSLATORS: This is an Ubuntu platform service for content exchange (ref. https://developer.ubuntu.com/en/phone/platform/guides/content-hub-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:78
msgid "Content Hub Handler"
msgstr "Manipulator Hub de conținut"

#. TRANSLATORS: This is an Ubuntu platform service for push notifications (ref. https://developer.ubuntu.com/en/phone/platform/guides/push-notifications-client-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:92
msgid "Push Helper"
msgstr "Helper pentru Push"

#. TRANSLATORS: i.e. Online Accounts (ref. https://developer.ubuntu.com/en/phone/platform/guides/online-accounts-developer-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:106
msgid "Accounts provider"
msgstr "Furnizor de conturi"

#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:46
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:54
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:60
msgid "Pick"
msgstr "Alege"

#~ msgid "AppArmor profile"
#~ msgstr "Profil AppArmor"

#~ msgid ""
#~ "OpenStore allows installing unconfined applications. Please make sure "
#~ "that you know about the implications of that."
#~ msgstr ""
#~ "OpenStore permite instalarea aplicațiilor fără limitări. Te rugăm să te "
#~ "asiguri că înțelegi implicațiile acestui lucru."

#~ msgid ""
#~ "An unconfined application has the ability to break the system, reduce its "
#~ "performance and/or spy on you."
#~ msgstr ""
#~ "O aplicație fără limitări poate să strice sistemul de operare, să-i "
#~ "reducă performanțele și/sau să te spioneze."

#~ msgid ""
#~ "While we are doing our best to prevent that by reviewing applications, we "
#~ "don't take any responsibility if something bad slips through."
#~ msgstr ""
#~ "Cu toate că noi facem tot posibilul să prevenim acest lucru prin "
#~ "verificarea aplicațiilor, nu ne asumăm nicio responsabilitate dacă ceva "
#~ "rău ne scapă."

#~ msgid "Use this at your own risk."
#~ msgstr "Folosește asta pe propriul risc."

#~ msgid "Okay. Got it! I'll be careful."
#~ msgstr "De acord. Am înțeles! O să am grijă."

#~ msgid "Developers"
#~ msgstr "Dezvoltatori"

#~ msgid "Manage your apps on OpenStore"
#~ msgstr "Gestionează aplicațiile tale din OpenStore"
